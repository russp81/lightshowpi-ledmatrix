#!/usr/bin/env python
#
# Licensed under the BSD license.  See full license in LICENSE file.
# http://www.lightshowpi.com/
#
# Author: Tom Enos (tomslick.ca@gmail.com)


"""wrapper module for Bibliopixel for use with lightshowpi

"""
import importlib
from bibliopixel.led import *
from bibliopixel.drivers.driver_base import *
from bibliopixel.drivers.serial_driver import *
import bibliopixel.colors as colors
from bibliopixel import log
import numpy


log.setLogLevel(log.WARNING)
#log.setLogLevel(log.DEBUG)

color_map = [colors.hue2rgb(c) for c in range(256)]
scale = colors.color_scale


class Led(object):
    
    def __init__(self, driver_type, led_number, channel_order, spi_speed):
        self.last = led_number - 1
        self.test = False
        channel_order = getattr(ChannelOrder, channel_order)
        self.main_driver = importlib.import_module("bibliopixel.drivers." + driver_type)
        self.my_driver = getattr(self.main_driver, "Driver" + driver_type)
        self.driver = self.my_driver(num=led_number,
                                     c_order=channel_order,
                                     use_py_spi=True,
                                     SPISpeed=spi_speed)
        
        self.led = LEDStrip(self.driver)
        
    def write(self, pin, color):

        self.led.set(pin, scale(color_map[color], color))
        
        if pin == self.last or self.test:
            self.led.update()

    def write2(self, levels):
        l = self.led.set
        for pin in xrange(len(levels)):
            color = scale(color_map[int(levels[pin])], int(levels[pin]))
            l(pin, color)
        self.led.update()


class LedSerial(object):
    def __init__(self, driver_type, led_channels, led_per_channel, color_order, max_brightness, led_update_throttle, p_color_map, p_color, p_type):
        led_number = led_channels * led_per_channel
        self.last = led_channels - 1
        self.lpc = led_per_channel
        self.skip = led_update_throttle * led_channels
        self.update_skip = self.skip 
	self.max_b = max_brightness / 100.0
        channel_order = getattr(ChannelOrder, color_order)
	driver_type = getattr(LEDTYPE, driver_type) 
        self.driver = DriverSerial(num=led_number,
                                   c_order=channel_order,
                                   type=driver_type)
        
        self.led = LEDStrip(self.driver)
#        self.led = LEDStrip(self.driver, threadedUpdate = True)
	self.led.setMasterBrightness(int(self.max_b * 255))

        self.pcolor = p_color
        self.pmap = p_color_map
        self.ptype = p_type
        
    def write(self, pin, color):

        self.led.set(pin, scale(color_map[color], color))
        
        if pin == self.last:
            self.led.update()

    def write2(self, levels):
        l = self.led.set
        for pin in xrange(len(levels)):
            color = scale(color_map[int(levels[pin])], int(levels[pin]))
            l(pin, color)
        self.led.update()


    def writefill(self,pin,level):
        if self.update_skip <> 0:
            self.update_skip = self.update_skip - 1
            if self.update_skip >= 0:
                return

        brightness = int(level * 255)
        sled = pin * self.lpc

        if self.pmap == 'MONO': 
            rgb = (int(level * self.pcolor[0]) ,int(level * self.pcolor[1]) ,int(level * self.pcolor[2]))
        elif self.pmap == 'FREQ1':
            rgb = color_map[int((float(pin)/(self.last+1)) * 255)]
            rgb = (int(rgb[0] * level), int(rgb[1] * level), int(rgb[2] * level))
        elif self.pmap == 'MAP1':
            rgb = scale(color_map[brightness],brightness)
        elif self.pmap == 'MAP2':
            rgb = scale(color_map[255 - brightness],brightness)
        else:
            rgb = (brightness,brightness,brightness)

        if self.ptype == 'CBARS':
            midl = int(self.lpc / 2)
            mlvl = int ( level * midl )
            self.led.fill((0,0,0), sled, sled + self.lpc - 1) 
            self.led.fill(rgb, sled + midl - mlvl , sled + midl + mlvl )
        elif self.ptype == 'FULL':
            self.led.fill(rgb, sled, sled + self.lpc - 1)

        if pin == self.last:
            self.led.update()
            self.update_skip = self.skip 

        
